from distutils.core import setup

setup(
    name='ManuPy',
    version='0.5.2',
    description='Python Utilities for Manuel Simon',
    author='Manuel Simon',
    author_email='simon.manuel27@gmail.com',
    packages=['manupy']
)