import math
import hashlib
import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from parse import *
from parse import Result


pltkwargs = {'linestyle':' ', 'marker':'.'}
linekwargs = {'linestyle':'-', 'linewidth':0.35/25.4*72}
errkwargs = {
    "linestyle":" ",   "marker":".",
    "capsize":0.6/25.4*72,     "markersize":0.75/25.4*72,
    "elinewidth":0.2/25.4*72, "capthick":0.25/25.4*72}
pltsamplep = 513
pltcolors = [
    "#004E8A", "#B1BD00", "#B90F22", "#009D81",
    "#F5A300", "#721085", "#004E73", "#AE8E00",
    "#732054", "#AFCC50", "#EE7A34"
]


def init_matplotlib():
    """
    Does some initialization settings in matplotlib, like setting the font
    and default colors for plots. Should be called once when matplotlib is imported.
    """
    plt.rc("font", size=10, family='Noto Sans')
    plt.rc("axes", prop_cycle=(plt.cycler("color", pltcolors)))


def parse_file(
    path: str, string: str, repl: list = list(),
    indexcol: str =None, print_unparsable: bool=False
) -> pd.DataFrame:
    """
    Parses a file by extracting the values belonging to the named arguments of the
    format string string.

    This means, every line of the file is parsed with the parse function of the parse
    package against string. If the line does not comply with the format string,
    it i skipped, else the named arguments of the format string (the unnamed ones
    are ignored) are stored as one row of a pandas DataFrame, where the column names
    corresond to the names of the arguments.

    Before doing this, several replacements may be done, specified by repl.

    path -- The path to the file

    string -- The format string agains which to parse the file.

    repl -- A list of (old, new) - pairs specifiyng replacement rules of
    old to new. old may be a regular expression.

    indexcol -- If given and identical to a named argument of string,
    these values are not stored in a regular DataFrame column but are used as the index
    of the DataFrame.

    print_urparsable -- If true, lines that do not comply with string are printed out.
    """
    f = open(path, 'rt')
    res = list()
    i = 0
    for line in f:
        for (old, new) in repl:
            line = line.replace(old, new)
        entr = parse(string, line)
        if type(entr) is Result:
            if not indexcol == None:
                ind = entr.named.pop(indexcol)
            else:
                ind = i
            res.append(pd.DataFrame(entr.named, index=[ind]))
            i += 1
        elif print_unparsable:
            print(line)
    f.close()
    if len(res) > 0:
        return pd.concat(res, 0)
    else:
        return pd.DataFrame()

def to_si(x: float, xun: float, prec_un: int = 1, omitb=False):
    """
    Returns a representation of a number with uncertainty that is suitable for
    using it in the siunitx LaTeX package.

    The number is rounded to a precision such that prec_un uncertain digits remain.
    The uncertainty is rounded to the corresponding precision and represented
    in the form "([uncertainty of digits])".

    The output is given in the form "{[value] ([uncertainty])}", i.e. enclosed
    in curly brackets such that it can directly be inserted after a \\SI or
    \\num command.

    x -- The value to represent

    xun -- The Uncertainty of the value

    prec_un -- The number of uncertain digits that shall be represented.
    Default is 1.
    """
    xun_dis = math.fabs(xun)
    magx = math.floor(math.log(math.fabs(x), 10))
    magxun = math.floor(math.log(xun_dis, 10))
    prec = magx - magxun + prec_un
    prec = max(prec, 1)
    xun_dis = math.ceil(xun_dis/10**(magxun-prec_un+1))
    if magx >= 4 or magx <= -3 or magx >= prec:
        res =  r"{x:.{prec:d}f} ({xun:d}) e{magx:d}".format(
            x=x/10**magx, xun=xun_dis, prec=prec-1, magx=magx
        )
        if omitb:
            return res
        else:
            return '{' + res + '}'

    else:
        res = r"{x:.{prec:d}f} ({xun:d})".format(
            x=x, xun=xun_dis, prec=prec-1-magx
        )
        if omitb:
            return res
        else:
            return '{' + res + '}'

def block_diag(x):
    """
    Returns a block matrix that contains the elements of x
    as blocks on its diagonal.

    x -- iterable of n_i x n_i matrices.
    """
    lengs = np.array([xt.shape[-1] for xt in x])
    return np.vstack(tuple(
        np.hstack((
            np.zeros((xt.shape[0], np.sum(lengs[:i]))),
            xt,
            np.zeros((xt.shape[0], np.sum(lengs[i+1:])))
        ))
        for (i, xt) in enumerate(x)
    ))

def cm_to_inch(x):
    try:
        return [it/2.54 for it in x]
    except:
        return x/2.54

def cache_func(f, dir, *args, repr_func=None):
    h = hashlib.sha256()
    if repr_func == None:
        for it in args:
            h.update(repr(it).encode())
    else:
        for (it, f) in zip(args, repr_func):
            if f == None:
                h.update(repr(it).encode())
            else:
                h.update(f(it))

    if os.path.exists(os.path.expanduser(dir + '_' + h.hexdigest())):
        with open(os.path.expanduser(dir + '_' + h.hexdigest()), "rb") as file:
            res = pickle.load(file)
        return res
    else:
        res = f(*args)
        with open(os.path.expanduser(dir + '_' + h.hexdigest()), "wb") as file:
            pickle.dump(res, file)
        return res

def extract_valun(x: tuple, index=0, factor=1.):
    (val, cov) = x
    un = np.sqrt(np.diag(cov))
    return (val[index]*factor, un[index]*factor)
