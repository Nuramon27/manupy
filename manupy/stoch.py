import numpy as np
import scipy
from scipy import constants
import mpmath

def gauss(x, μ, σ, A):
    """
    The gaussian normal distribution.
    """
    return (A/(np.sqrt(2*constants.pi)*σ))*np.exp(-np.power(x-μ, 2)/(2*σ**2))

def max_sigma_to_A(σ, max):
    return max*(np.sqrt(2*constants.pi)*σ)

def gauss_prime(x, μ, σ, A):
    """
    The first derivative of the gaussian normal distribution.
    """
    return (x-μ)/σ**2*gauss(x, μ, σ, A)

def mgauss(x, μ, σ, A, t, m):
    """
    A gaussian normal distribution with an additional linear background
    """
    return gauss(x, μ, σ, A)  + t + m*x

def mgauss_prime(x, μ, σ, A, t, m):
    """
    The derivative of a gaussian normal distribution with an additional linear background
    """
    return m - (x-μ)/σ**2*gauss(x, μ, σ, A)

def lorentz(ω, ω0, γ, A):
    return A * γ**2 * ω0**2/(np.power((np.power(ω, 2) - ω0**2), 2) + γ**2 * ω0**2)

def cauchy(x, μ, s, A):
    return A * s/constants.pi * (s/2) / (np.power(x - μ, 2) + (s/2)**2)


def posterior(datr, bacr, dattime, bactime):
    dat = mpmath.mpmathify(1)*datr
    bac = mpmath.mpmathify(1)*bacr
    r = mpmath.mpmathify(dattime/bactime)
    Ω = np.array([mpmath.gammainc(dati + 1, r * baci) for (dati, baci) in zip(dat, bac)])
    P = np.array([mpmath.gammainc(dati + 2, r * baci) - r * baci * Ωi for (dati, baci, Ωi) in zip(dat, bac, Ω)])

    return np.array(P/Ω, dtype=np.float64)
