import numpy as np
from numpy import linalg


def linearregression_weighted(f, x, y, g):
    AtgA = np.array([[
        sum(np.conj(kt(jt))*g[j]*lt(jt) for (j, jt) in enumerate(x)) for lt in f
    ] for kt in f])
    Atgy = np.array([sum(
        np.conj(it(x[j])) * g[j] * y[j] for j in range(len(x))
    ) for it in f])
    w = linalg.solve(AtgA, Atgy)
    return w

def linearregression_uncert_weighted(f, x, y, g, Δy):
    AtgA = np.array([[
        sum(np.conj(kt(jt))*g[j]*lt(jt) for (j, jt) in enumerate(x)) for lt in f
        ] for kt in f])
    Atgainv = linalg.inv(AtgA)
    Atg = np.array([[np.conj(it(x[j])) * g[j] for j in range(len(x))] for it in f])
    return np.sqrt(np.dot(np.power(np.matmul(Atgainv, Atg), 2), np.power(Δy, 2)))

def curve_fit_precmat(f, xdata, ydata, yundata, p, p_step=1./2**10):
    U = np.zeros((len(p), len(p)))
    base = p_step*np.identity(len(p))
    for (x, y, yun) in zip(xdata, ydata, yundata):
        Df = np.array([(
            f(x, *(p*(1. + b))) - f (x, *(p*(1. - b)))
        )/(pt*p_step) for (pt, b) in zip(p, base)])
        Hf = np.array([[
            (
                f(x, *(p*(1. + b1 + b2))) - f(x, *(p*(1. + b1 - b2))) +
                f(x, *(p*(1. - b1 - b2))) - f(x, *(p*(1. - b1 + b2)))
            )/(pt*ptt*p_step**2) for (ptt, b2) in zip(p, base)
        ] for (pt, b1) in zip(p, base)])
        U += Hf*(f(x, *p) - y)/np.square(yun)
        U += np.outer(Df, Df)/np.square(yun)

    return (U + np.transpose(U))/2.

# def curve_fit_uncert(paramindex: int, f, xdata, ydata, yundata, p, p_step=1/2**12):
#     U = curve_fit_precmat(f, xdata, ydata, yundata, p, p_step)
#     eig = linalg.eigh(U)
#     Usqrt = np.dot(eig[1] / np.sqrt(eig[0]), np.transpose(eig[1]))
#     return np.sqrt(Usqrt)

def pass_uncert(f, x, xun, step=1./2.**12, absolute_step=False):
    """
    Calculates the covariance matrix of f(x) based on the gaussian propagation
    of uncertainties.

    f -- callable.

    The function through wich the uncertainties should propagate

    is expected to have the form "(array of dimension) n -> array of dimension m"

    x -- A single value for the argument of x or an array of those.

    If an array of values is given, f(x) must also return an array of arrays
    of dimension m.

    xun -- The covariance matrix of x. If an array is given for x,
    this must also be an array of covariance matrices.

    step -- The (relative) grid size with wich the derivative of f will
    be calculated numerically.

    absolute_step -- If True, step is treated as absolute, i.e. the derivative
    of f is calculated as (f(x+step) - f(x))/step.

    If False, step is treated as relative, i.e. the derivative of f is calculated
    as (f(x*(1+step)) - f(x))/(x*step).

    Default is False, but if x contains zeros, then absolute must be chosen.
    """
    base = step*np.identity(np.shape(x)[-1])
    if absolute_step:
        Df = np.moveaxis(np.array([(
            f(x + b) - f(x - b)
        ) for b in base]), 0, -1)/(step)
    else:
        Df = np.moveaxis(np.array([(
            f(x*(1. + b)) - f(x*(1. - b))
        ) for b in base]), 0, -1)/(x*step)

    return np.dot(np.dot(Df, xun), np.swapaxes(Df, -1, -2))

def val_with_un(f, x, xun, step=1./2.**12, absolute_step=False):
    """
    Calculates the covariance matrix of f(x) based on the gaussian propagation
    of uncertainties and returns it together with f(x)

    f -- callable.

    The function through wich the uncertainties should propagate

    is expected to have the form "(array of dimension) n -> array of dimension m"

    x -- A single value for the argument of x or an array of those.

    If an array of values is given, f(x) must also return an array of arrays
    of dimension m.

    xun -- The covariance matrix of x. If an array is given for x,
    this must also be an array of covariance matrices.

    step -- The (relative) grid size with wich the derivative of f will
    be calculated numerically.

    absolute_step -- If True, step is treated as absolute, i.e. the derivative
    of f is calculated as (f(x+step) - f(x))/step.

    If False, step is treated as relative, i.e. the derivative of f is calculated
    as (f(x*(1+step)) - f(x))/(x*step).

    Default is False, but if x contains zeros, then absolute must be chosen.
    """
    return (f(x), pass_uncert(f, x, xun, step, absolute_step))
